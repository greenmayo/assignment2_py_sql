"""
Assignment 2 for Python class, Part 2: File load
@author Parhomenco Kirill 1933830

Note: Had to modify the csv file to have appropriate headers (used the file
from one of the previous labs, titanic.csv, otherwise pandas dataframe would count Mr.Owen
as a header for the names column.
"""
import mysql.connector
from mysql.connector import Error
import pandas as pd

try:
    connection = mysql.connector.connect(
        host="localhost",
        user="root",
        password="*******",  # I will not give you my password!
        database="lab03"
    )
    if connection.is_connected():
        db_Info = connection.get_server_info()
        print("Connected to MySQL Server version ", db_Info)
        cursor = connection.cursor()
        cursor.execute("select database();")
        record = cursor.fetchone()
        print("You're connected to database: ", record)
        my_cursor = connection.cursor(buffered=True)

        # 1 loading csv into python
        titanic_data = pd.read_csv('titanicData.csv')
        df = pd.DataFrame(titanic_data)

        # 4 dropping titanic if exists
        my_cursor.execute("drop table if exists titanic;")

        # 3 creating a passenger table
        my_cursor.execute("""create table titanic
                            (   survived boolean null,
                                p_class int null,
                                name varchar(255) null,
                                sex varchar(10) null,
                                age double null,
                                siblings_spouses int null,
                                par_child int null,
                                fare double null          );""")

        # 5 inserting dataframe into SQL
        # does not work with "{}".format() because of ' inside passenger names, they break the quotations
        # so %s was used
        sql = "insert into lab03.titanic(survived, p_class, name, sex, age, siblings_spouses, par_child, fare)\
                            values(%s,%s,%s,%s,%s,%s,%s,%s);"
        for row in df.itertuples():
            my_cursor.execute(sql, (
                                row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8]))
        connection.commit()

        # 6 Average death rate
        # with pandas we could use df["Survived"].mean() to get 0.385569 or 38.55% survival rate.
        # let's see if that's the case with SQL
        my_cursor.execute("select avg(survived)*100 from titanic;")
        for i in my_cursor:
            # converting the i tuple to string, splitting it using ' surrounding the number
            # since it is at position 2 -> [1]
            # turning string '0.3856' to float, multiplying by 100 to get %
            mean_value = round(float((str(i).split('\'')[1])), 2)
            # mean_value *= 100
            print("Average rate of death overall is {}%".format(mean_value))

        # 7 death rate by class
        for x in (1, 2, 3):
            my_cursor.execute("select avg(survived)*100 from titanic where p_class = {}".format(x))
            for i in my_cursor:
                # reusing code above, not the best practice but it is too small to worry about that
                mean_value = round(float((str(i).split('\'')[1])), 2)
                print("Average rate of death in the {} class is {}%".format(x, mean_value))



except Error as e:
    print("Error while connecting to MySQL: ", e)
finally:
    if connection.is_connected():
        cursor.close()
        connection.close()
        print("MySQL connection is closed")