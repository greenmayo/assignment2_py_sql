"""
Assignment 2 for Python class, Part 1: Pet shop
@author Parhomenco Kirill 1933830

Note: One would probably need to have a lab03 schema to work with this script,
I realised this flaw too far into work and don't thinks it would be a major issue
I am willing to attach a pure sql script of the commands run by this script, just in case
"""

import mysql.connector
from mysql.connector import Error

try:
    connection = mysql.connector.connect(
        host="localhost",
        user="root",
        password="*******",  # I will not give you my password!
        database="lab03"
    )
    if connection.is_connected():
        db_Info = connection.get_server_info()
        print("Connected to MySQL Server version ", db_Info)
        cursor = connection.cursor()
        cursor.execute("select database();")
        record = cursor.fetchone()
        print("You're connected to database: ", record)
        my_cursor = connection.cursor(buffered=True)

        # 0 dropping table if exist to avoid errors - also number 15
        my_cursor.execute("drop table if exists pet_owner, pet;")

        # 1 creating table pet and setting up the columns, pet_id is a primary key
        my_cursor.execute("create table pet \
                            (pet_name varchar(30) null,\
                            animal_type varchar(100) null,\
                            owned boolean default false null,\
                            pet_id int not null,\
                            constraint pet_pk\
                            primary key (pet_id)\
                            );")

        # 2 added default on table creation, but here is the code for standalone modification
        # 3 my_cursor.execute("alter table pet alter owned set default false;")

        # 4 inserting pets, I find it cleaner to create an array and loop through it
        pet_data = [
            "INSERT INTO lab03.pet (pet_name, animal_type, owned, pet_id) VALUES ('Dobbie', 'elf', DEFAULT, 1);",
            "INSERT INTO lab03.pet (pet_name, animal_type, owned, pet_id) VALUES ('Sneaky', 'cat', DEFAULT, 2);",
            "INSERT INTO lab03.pet (pet_name, animal_type, owned, pet_id) VALUES ('Harvey', 'dog', DEFAULT, 3);",
            "INSERT INTO lab03.pet (pet_name, animal_type, owned, pet_id) VALUES ('Spot', 'dog', DEFAULT, 4);",
            "INSERT INTO lab03.pet (pet_name, animal_type, owned, pet_id) VALUES ('Snowball', 'cat', DEFAULT, 5);",
            "INSERT INTO lab03.pet (pet_name, animal_type, owned, pet_id) VALUES ('Asti', 'cat', DEFAULT, 6);",
            "INSERT INTO lab03.pet (pet_name, animal_type, owned, pet_id) VALUES ('Tonka', 'cat', DEFAULT, 7);",
            "INSERT INTO lab03.pet (pet_name, animal_type, owned, pet_id) VALUES ('Pirate', 'parrot', DEFAULT, 8);",
            "INSERT INTO lab03.pet (pet_name, animal_type, owned, pet_id) VALUES ('Lizzie', 'lizard', DEFAULT, 9);",
            "INSERT INTO lab03.pet (pet_name, animal_type, owned, pet_id) VALUES ('Blob', 'fish', DEFAULT, 10);"
        ]
        for pets in pet_data:
            my_cursor.execute(pets)
        connection.commit()  # does not update the table without this line

        # 5 adding cost column to the table
        my_cursor.execute("alter table pet add cost double null;")

        # 6 making pet_name be 100 characters long
        my_cursor.execute("alter table lab03.pet modify pet_name varchar(100);")

        # 7 adding 3 new pets
        my_cursor.execute("INSERT INTO lab03.pet (pet_name, animal_type, owned, pet_id, cost) VALUES ('Garlic', 'rat', DEFAULT, 11, 90);")
        my_cursor.execute("INSERT INTO lab03.pet (pet_name, animal_type, owned, pet_id, cost) VALUES ('Reynor', 'hamster', DEFAULT, 12, 80);")
        my_cursor.execute("INSERT INTO lab03.pet (pet_name, animal_type, owned, pet_id, cost) VALUES ('Tychus', 'dog', DEFAULT, 13, 70);")
        connection.commit()

        # 8 updating records with no cost
        my_cursor.execute("update lab03.pet set cost = 50 where cost is null;")

        # 9 deleting all dogs
        my_cursor.execute("delete from lab03.pet where animal_type = 'dog';")

        # 10 creating pet_owner table
        my_cursor.execute("create table pet_owner\
                            (\
                                owner_name varchar(30) null,\
                                pet_id int null,\
                                constraint pet_owner_pet_pet_id_fk\
                                    foreign key (pet_id) references pet (pet_id)\
                            );")

        # 11 inserting 3 new owners and 3 new pets
        new_inhabitants = [
            "INSERT INTO lab03.pet (pet_name, animal_type, owned, pet_id, cost) VALUES ('Ashe', 'rabbit', DEFAULT, 14, 25);",
            "INSERT INTO lab03.pet (pet_name, animal_type, owned, pet_id, cost) VALUES ('Orisa', 'horse', DEFAULT, 15, 300);",
            "INSERT INTO lab03.pet (pet_name, animal_type, owned, pet_id, cost) VALUES ('Reinhardt', 'armadillo', DEFAULT, 16, 250);",
            "INSERT INTO lab03.pet_owner (owner_name, pet_id) VALUES ('Harvey Porter', 1);",
            "INSERT INTO lab03.pet_owner (owner_name, pet_id) VALUES ('Leroy Jenkins', 5);",
            "INSERT INTO lab03.pet_owner (owner_name, pet_id) VALUES ('Harry Houdini', 7);",
        ]
        for item in new_inhabitants:
            my_cursor.execute(item)
        connection.commit()

        # 12 Let's find homes for our pets
        spca_blessing = [
            "INSERT INTO lab03.pet_owner (owner_name, pet_id) VALUES ('Harvey Porter', 2)",
            "INSERT INTO lab03.pet_owner (owner_name, pet_id) VALUES ('Harvey Porter', 6)",
            "INSERT INTO lab03.pet_owner (owner_name, pet_id) VALUES ('Harvey Porter', 8)",
            "INSERT INTO lab03.pet_owner (owner_name, pet_id) VALUES ('Harvey Porter', 9)",
            "INSERT INTO lab03.pet_owner (owner_name, pet_id) VALUES ('Leroy Jenkins', 16)",
            "INSERT INTO lab03.pet_owner (owner_name, pet_id) VALUES ('Leroy Jenkins', 11)",
            "INSERT INTO lab03.pet_owner (owner_name, pet_id) VALUES ('Leroy Jenkins', 12)",
            "INSERT INTO lab03.pet_owner (owner_name, pet_id) VALUES ('Harry Houdini', 14)",
            "INSERT INTO lab03.pet_owner (owner_name, pet_id) VALUES ('Harry Houdini', 15)",
            "INSERT INTO lab03.pet_owner (owner_name, pet_id) VALUES ('Harry Houdini', 10)",
        ]
        for blessing in spca_blessing:
            my_cursor.execute(blessing)
        connection.commit()
        # and make the db check automatically if the pet is owned and change that
        my_cursor.execute("update pet p, pet_owner po set p.owned = TRUE where p.pet_id = po.pet_id;")
        connection.commit()

        # 13 Amount spent on pets
        my_cursor.execute("select po.owner_name, sum(p.cost) from pet p "
                          "join pet_owner po on p.pet_id = po.pet_id group by po.owner_name;")
        for x in my_cursor:
            print("{} has spent {}".format(x[0], x[1]))

        # 14 Pets in the biggest family
        my_cursor.execute("select p.pet_name from pet p\
                            join pet_owner po on p.pet_id = po.pet_id\
                            where owner_name = (select owner_name from pet_owner\
                                group by owner_name order by count(owner_name) desc\
                                limit 1);")
        for x in my_cursor:
            print(x[0])

except Error as e:
    print("Error while connecting to MySQL: ", e)
finally:
    if connection.is_connected():
        cursor.close()
        connection.close()
        print("MySQL connection is closed")
